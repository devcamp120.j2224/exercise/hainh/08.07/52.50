
import java.util.ArrayList;
import java.util.Date;
import com.devcamp.j03.Order;

public class App {

    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        ArrayList<Order> arraylist = new ArrayList<Order>();

        Order order1 = new Order(); 
        Order order2 = new Order("Hải");
        Order order3 = new Order(10,"Hoang Hai",100000);
       
        Order order4 = new Order(9 ,"Nguyen Hoang Hai",200000,new Date() ,false,new String[]{"hủ tiếu" ,"phở" ,"cơm chiên"});

        arraylist.add(order1);
        arraylist.add(order2);
        arraylist.add(order3);
        arraylist.add(order4);
        
        for (Order order : arraylist){
            System.out.println(order.toString());
        }
    }
}
