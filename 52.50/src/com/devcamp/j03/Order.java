package com.devcamp.j03;
import java.util.Locale;
import java.util.Arrays;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


            public class Order {
            public int id; // id của order
            public String customer; // tên khách hàng
            public long price;// tổng giá tiền
            public Date date = new Date(); // ngày thực hiện order
            public boolean confirm; // đã xác nhận hay chưa?
            public String[] items; // danh sách mặt hàng đã mua

            public Order(String customer){
                this.id = 1 ;
                this.customer = customer ;
                this.price = 1200000;
                this.date = new Date();
                this.confirm = true; 
                this.items = new String []{"tv" ,"imac," , "ipad"};
            }
            public Order (int id , String customer , long price , Date date , boolean confirm , String[] items){
                this.id = id ;
                this.customer = customer ;
                this.price = price ; 
                this.date = date ;
                this.confirm = true ; 
                this.items = new String[]{"tv" ,"imac," , "ipad"};
            }

            public Order(){
                System.out.println("ko tham số");
            }

            public Order(int id , String customer , long price){
                this(id , customer , price , new Date() , true , new String[]{"panasonic","sony","sanyo"});
            }
            // ghi đè 
            @Override

            public String toString(){
                // định dạng tiêu chuẩn VN
                Locale.setDefault(new Locale("vi" , "VN"));
                // định dạng cho ngày tháng

                // CÁCH 2 
                // LocalDateTime localDate = LocalDateTime.now(); // fixed: LocalDateTime
                // DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MMM-dd hh:mm:ss");
                 
                // CÁCH 1 
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss.SSS");

                // định dạng cho giá tiền 
                Locale locate = Locale.getDefault();
                NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locate);
                // return (trả ra ) chuỗi String

                return  "Order [id=" + id 
                + ", customer : " + customer 
                + ", price : " + numberFormat.format(price) 
                + ", date : " + formatter.format(date)
                + ", confirm: " + confirm
                + ", items : " + Arrays.toString(items);

            }
        }
